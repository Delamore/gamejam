using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class ContextMenuItem : MonoBehaviour
{
    public ContextAction containedAction;
    public TMP_Text nameText;
    public TMP_Text costText;
    public Image symbolDisplay;
    public ContextualMenu parentMenu;
    public bool actionAvaliable;
    public static int height = 25;
    public void Set(ContextualMenu menu, ContextAction action)
    {
        containedAction = action;
        actionAvaliable = containedAction.IsAvaliable();
        nameText.text = action.name;
        costText.text = action.actionPointCost.ToString();
        if (action.iconForRequiredItem == null)
            symbolDisplay.gameObject.SetActive(false);
        else
            symbolDisplay.sprite = action.iconForRequiredItem;

        if(action.isAvaliableFunc())
        {
            GetComponent<Button>().onClick.AddListener(new UnityAction(OnClick));
        }
        else
        {
            nameText.color = Color.grey;
        }

        parentMenu = menu;
    }
    public void OnClick()
    {
        containedAction.actionFunc.Invoke();
        AllActions.ActionInvoked(containedAction);
        ContextualMenu.currentMenu.Close();
    }
}
