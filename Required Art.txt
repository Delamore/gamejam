Tools:
-Shovel
-Hoe

Background:
-Grass
-Garden area
-Fence

Plants:
-Any you feel like doing, but need at least a couple
-one or more weeds

-The neighbours
-Possibly the player or reuse neighbour
-Animations for gardening, stealing a plant and destroying a garden tile
animations can just be shaking the sprite with various intensities depending on the action

UI:
-Sun clock which consists of a sky background, the ground and the sun
all 3 must be separate for code to work properly as ground hides the sun by being in the foreground

-SFX mute button
-Music mute button
-SFX volume bar
-Music volume bar

Can move both the above to a pause UI giving you more freedom with shape / size

-End day button

-Action point counter

This hasn't been done other than just a text so far, but feels like somewhere we could 
add some polish. I register when the ticks for action point loss would happen even if the
action takes more than 1 point so it's possible to animate points as they're spent in time
or just animate the total cost spent at once.

Can use premade UI assets like kenneys packs for everything except the sun clock and action point counter
if required

