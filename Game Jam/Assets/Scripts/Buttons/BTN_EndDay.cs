using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BTN_EndDay : MonoBehaviour
{
    bool coolDown = false;
    void Start()
    {
        Button buttonAttached = GetComponent<Button>();
        buttonAttached.onClick.RemoveAllListeners();
        buttonAttached.onClick.AddListener(delegate { Pressed(); });
    }
    IEnumerator CoolDown()
    {
        coolDown = true;
        yield return new WaitForSeconds(0.2f);
        coolDown = false;
    }
    private void Pressed()
    {
        if (coolDown || GameManager.instance.InputBlocked()) return;
        GameManager.instance.ForceEndDay();
        StartCoroutine(CoolDown());
    }
}
