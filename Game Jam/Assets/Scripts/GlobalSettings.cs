﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class GlobalSettings
{
    #region audio
    public static float musicVolume = 1f;
    public static float SFXVolume = 1f;
    #endregion

    #region filepaths
    public const string plantPrefabPath = "Prefabs/Plants";
    #endregion

    public const int maxActionPoints = 24;
    public const int startHour = 8;
    public const int endHour = 20;

    public const float timeToAnimateFullDayOver = 20;


    #region Fade to dark settings
    public const float fadeToDarkTime = 1f;
    public const float holdDarkTime = 0.5f;
    public const float fadeToDayTime = 1f;
    #endregion


    #region debug headers
    //public const string EventManagerDebugHeader = "<color=yellow>☀ </color><color=#72d47f><b>Event:</b></color>";
    //public const string RunOnStartDebugHeader = "<color=yellow>→ </color><color=cyan><b>RunOnStart:</b></color>";
    public const string NeighbourDebugHeader = "<color=yellow>⌺ <b><color=#73a1de>Neighbour:</color></b> </color>";
    public const string ActionPointsDebugHeader = "<color=yellow>⌺ <b><color=green>Action Points:</color></b> </color>";
    public const string GameManagerDebugHeader = "<color=yellow>♦ <b><color=lightblue>GameManager:</color></b> </color>";
    public const string AudioDebugHeader = "<color=yellow>♫ <b><color=#aa73bf>Audio:</color></b> </color>";
    //public const string UIDebugHeader = "<color=yellow>▣ <b><color=#d4d28e>UI:</color></b> </color>";
    //public const string TrackDebugHeader = "<color=yellow>≡ <b><color=#6f818c>Track:</color></b> </color>";
    public const string GardenDebugHeader = "<color=yellow>∾ </color><color=#798bf2><b>Garden:</b></color>";
    #endregion
}
