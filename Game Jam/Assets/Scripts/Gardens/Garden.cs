using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Garden : MonoBehaviour
{
    [Header("Set In Inspector")]
    public int width;
    public int height;
    public GameObject gardenTilePrefab;
    public float gardenTileSize = 0.5f;
    public GameObject sizeVisualizer;

    [Header("Set by game")]
    public List<Vector2Int> unoccupiedTiles;
    public GardenTile[,] gardenTiles;
    public float totalValue = 0;
    public Gardener owner;
    public int openTiles => unoccupiedTiles.Count;

    public Transform tileParent;
    public int totalPlants => (width * height) - openTiles; 

    #region init

    public void Setup(Gardener owner)
    {
        this.owner = owner;
        tileParent = new GameObject().transform;
        tileParent.gameObject.name = "Garden Tiles";
        tileParent.SetParent(transform,false);
        gardenTiles = new GardenTile[width, height];
        unoccupiedTiles = new List<Vector2Int>();
        SpawnGardenTiles();
        if (!owner.isPlayer) SpawnRandomPlants();
    }
    private void SpawnGardenTiles()
    {
        Vector2 startPosition = -new Vector2(width / 2f * gardenTileSize - gardenTileSize / 2f, height / 2f * gardenTileSize - gardenTileSize / 2f);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector2Int position = new Vector2Int(x, y);
                unoccupiedTiles.Add(position);

                GameObject tile = Instantiate(gardenTilePrefab);
                tile.transform.SetParent(tileParent);
                tile.transform.localPosition = new Vector3(startPosition.x + x * gardenTileSize, startPosition.y + y * gardenTileSize, 0);

                GardenTile gardenTile = tile.GetComponent<GardenTile>();
                gardenTile.Setup(position, this);
                gardenTiles[x, y] = gardenTile;
            }
        }
    }
    void SpawnRandomPlants()
    {
        int count = openTiles;//Random.Range(0, openTiles);
        for (int i = 0; i < count; i++)
        {
            int randomPlantIndex = Random.Range(0, GameManager.allPlantPrefabs.Length);
            Plant randomPlant = GameObject.Instantiate(GameManager.allPlantPrefabs[randomPlantIndex]);
            AddPlant(randomPlant);
        }
    }
    #endregion


    public GardenTile RandomUnoccupiedTile()
    {
        if (openTiles == 0) throw new System.Exception("No free tiles");
        int index = Random.Range(0, openTiles);
        Vector2Int tilePosition = unoccupiedTiles[index];
        return gardenTiles[tilePosition.x, tilePosition.y];
    }

    public void AddPlant(Plant plant)
    {
        GardenTile randomTile = RandomUnoccupiedTile();
        AddPlant(plant, randomTile);
    }
    public void AddPlant(Plant plant, GardenTile tile)
    {
        Debug($"Adding {plant.gameObject.name} to {tile.position}");
        OccupyTile(tile.position);
        totalValue += plant.value;
        tile.AddPlant(plant);
    }
    public void RemovePlant(GardenTile tile, bool destroy)
    {
        Debug($"Removing plant from {tile.position}");
        Plant plant = tile.containedPlant;
        totalValue -= plant.value;
        tile.RemovePlant();
        FreeTile(tile.position);

        if (destroy)
        {
            Debug($"Destroying plant {plant.gameObject.name}");
            //plant.gameObject.transform.position = Vector3.zero;
            Destroy(plant.gameObject);
        }
    }
    private void OccupyTile(Vector2Int position)
    {
        Debug($"Occupying tile {position}");
        unoccupiedTiles.Remove(position);
    }
    private void FreeTile(Vector2Int position)
    {
        Debug($"Freeing tile {position}");
        unoccupiedTiles.Add(position);
    }

    #region Debug
    private void OnValidate()
    {
        sizeVisualizer.transform.localScale = new Vector3(width / 2f, height / 2f, 1);
    }

    private void Debug(string message)
    {
        //UnityEngine.Debug.Log($"{GlobalSettings.GardenDebugHeader} Owner {owner.neighbourName} {message}");
    }
    #endregion
}
