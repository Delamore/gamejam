using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ContextualMenu 
{

    public static ContextualMenu currentMenu;

    static string contextMenuHeaderPath = "Prefabs/UI/CxtMenuHead";
    static string contextMenuActionPath = "Prefabs/UI/CxtMenuAction";

    GameObject headerPrefab;
    GameObject menuActionPrefab;

    List<ContextMenuItem> menuItems;

    List<GameObject> objects;
    Vector2 headerPosition;

    GameObject header;

    Canvas canvas => GameManager.instance.mainCanvas;

    public ContextualMenu(List<ContextAction> actions, Vector2 clickPosition)
    {
        currentMenu = this;
        LoadPrefabs();
        SetCorrectHeaderPosition(clickPosition, actions.Count);
        SpawnHeader();
        CreateAllMenuItems(actions);
    }
    
    void LoadPrefabs()
    {
        headerPrefab = Resources.Load<GameObject>(contextMenuHeaderPath);
        menuActionPrefab = Resources.Load<GameObject>(contextMenuActionPath);
    }
    void SpawnHeader()
    {
        header = GameObject.Instantiate(headerPrefab, canvas.transform);
        header.transform.position = headerPosition;
        objects = new List<GameObject>();
        objects.Add(header);
    }
    void SetCorrectHeaderPosition(Vector2 clickPosition, int actionCount)
    {
        float minHeight = ContextMenuItem.height * (actionCount+1);
        if (clickPosition.y < minHeight)
            headerPosition = new Vector2(clickPosition.x, minHeight);
        else
            headerPosition = clickPosition;
    }
    void CreateAllMenuItems(List<ContextAction> actions)
    {
        menuItems = new List<ContextMenuItem>();
        foreach (ContextAction action in actions)
            CreateMenuItem(action);

    }
    void CreateMenuItem(ContextAction action)
    {
        ContextMenuItem createdMenuItem = GameObject.Instantiate(menuActionPrefab, canvas.transform).GetComponent<ContextMenuItem>();
        menuItems.Add(createdMenuItem);

        headerPosition.y -= ContextMenuItem.height * 1f;
        createdMenuItem.transform.position = headerPosition;

        createdMenuItem.Set(this, action);

        objects.Add(createdMenuItem.gameObject);
    }

    public void Close()
    {
        currentMenu = null;
        GameManager.instance.StartCoroutine(StartDestruction());
    }
    private IEnumerator StartDestruction()
    {
        yield return new WaitForEndOfFrame();
        //for (int i = objects.Count - 1; i >= 0; i--)
        //    objects[i].SetActive(false);
        //yield return new WaitForSeconds(1f);
        for (int i = objects.Count - 1; i >= 0; i--)
            GameObject.Destroy(objects[i]);
    }
}
