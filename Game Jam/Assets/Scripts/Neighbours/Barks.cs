using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bark List", menuName = "Jam/Bark List", order = 51)]
public class Barks : ScriptableObject
{
    public List<string> leavingHome = new List<string>();
    public List<string> returningHome = new List<string>();
    public List<string> goingInside = new List<string>();
    public List<string> goingToSleep = new List<string>();
    public List<string> wakingUp = new List<string>();
    public List<string> goingOutside = new List<string>();
    public List<string> movingFromYardToGarden = new List<string>();
    public List<string> movingFromOutsideDoorToYard = new List<string>();
    public List<string> movingFromYardToOutsideDoor = new List<string>();
    public List<string> movingFromGardenToYard = new List<string>();

    public string GetBarkForAction(NeighbourAction action, Gardener gardener)
    {

        if (!ShouldBarkBePlayed(action, gardener)) return null;

        switch (action)
        {
            case NeighbourAction.Continue:
                return null;
            case NeighbourAction.Leave:
                return GetBarkFromList(leavingHome);
            case NeighbourAction.ComeHome:
                return GetBarkFromList(returningHome);
            case NeighbourAction.GoInside:
                return GetBarkFromList(goingInside);
            case NeighbourAction.GoToSleep:
                return GetBarkFromList(goingToSleep);
            case NeighbourAction.Wakeup:
                return GetBarkFromList(wakingUp);
            case NeighbourAction.GoOutside:
                return GetBarkFromList(goingOutside);
            case NeighbourAction.EnterGarden:
                return GetBarkFromList(movingFromYardToGarden);
            case NeighbourAction.EnterYard:
                return GetBarkFromList(movingFromOutsideDoorToYard);
            case NeighbourAction.LeaveYard:
                return GetBarkFromList(movingFromYardToOutsideDoor);
            case NeighbourAction.LeaveGarden:
                return GetBarkFromList(movingFromGardenToYard);
            default:
                throw new System.Exception($"No bark implemeted for {action}");
        }
    }

    private bool ShouldBarkBePlayed(NeighbourAction action, Gardener gardener)
    {
        return true;
    }

    private string GetBarkFromList(List<string> list)
    {
        int randomIndex = Random.Range(0, list.Count);
        return list[randomIndex];
    }
}
