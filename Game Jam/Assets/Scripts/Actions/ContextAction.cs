using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ContextAction 
{
    public string name = "Action";
    public Sprite iconForRequiredItem;
    public int actionPointCost = 2;

    public UnityAction actionFunc;
    public Func<bool> isAvaliableFunc;
    public ContextAction(string name, int cost, UnityAction actionFunc, Func<bool> isAvaliableFunc, Sprite icon = null)
    {
        this.name = name;
        this.actionPointCost = cost;
        this.actionFunc = actionFunc;
        this.iconForRequiredItem = icon;
        this.isAvaliableFunc = isAvaliableFunc;
    }
    public bool IsAvaliable()
    {
        return isAvaliableFunc();
    }

    public static ContextAction StealActionForGardenTile(GardenTile tile)
    {
        int actionCost = 3;
        string actionName = "Steal";
        Func<bool> isActionAvaliableFunc = new Func<bool>(() =>{
            return 
                tile.containedPlant != null && 
                GameManager.instance.PlayerCanStealPlant() && 
                ActionPoints.HasEnoughPoints(actionCost);
        });
        return new ContextAction(actionName, actionCost, tile.Steal, isActionAvaliableFunc, null);
    }
    public static ContextAction DestroyActionForGardenTile(GardenTile tile)
    {
        int actionCost = 2;
        string actionName = "Destroy";
        Func<bool> isActionAvaliableFunc = new Func<bool>(() =>{
            return 
                tile.containedPlant != null && 
                ActionPoints.HasEnoughPoints(actionCost);
        });
        return new ContextAction(actionName, actionCost, tile.Destroy, isActionAvaliableFunc, null);
    }
    public static ContextAction MoveToGarden(Gardener gardenOwner)
    {
        int actionCost = 1;
        string actionName = "Move to";
        Func<bool> isActionAvaliableFunc = new Func<bool>(() => {
            return
                ActionPoints.HasEnoughPoints(actionCost);
        });
        return new ContextAction(actionName, actionCost, gardenOwner.MoveToGarden, isActionAvaliableFunc, null);
    }

}
