using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenHitBox : MonoBehaviour
{
    public Garden gardenContaining;
    OnClick clickControl;
    List<ContextAction> actions;
    public void Awake()
    {
        clickControl = GetComponent<OnClick>();
        clickControl.onClickWithPosition = OnClick;
        actions = new List<ContextAction>
            {
                ContextAction.MoveToGarden(gardenContaining.owner),
            };
    }

    public void OnClick(Vector2 position)
    {
        Debug.Log("Clicked garden hitbox");
        new ContextualMenu(actions, position);
    }
}
