using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum NeighbourAction
{
    Continue = 0,
    Leave = 1,
    ComeHome = 2,
    GoInside = 3,
    GoToSleep = 4,

    Wakeup = 6,

    GoOutside = 8,
    EnterGarden = 9,
    EnterYard = 10,
    LeaveYard = 11,
    LeaveGarden = 12,
}
[Serializable]
[CreateAssetMenu(fileName = "New Schedule", menuName = "Jam/Schedule", order = 51)]
public class Schedule : ScriptableObject
{

    public NeighbourAction[] scheduledActions = new NeighbourAction[GlobalSettings.maxActionPoints];

    public NeighbourAction GetAction()
    {
        return scheduledActions[TimeOfDay.currentAction];
    }
    public NeighbourAction DoScheduledAction(Gardener gardener)
    {
        NeighbourAction scheduledAction = GetAction();
        Debug.Log($"<color=cyan>{gardener.neighbourName}</color> Doing action <color=green>{scheduledAction}</color> at schedule index {TimeOfDay.currentAction}");
        switch (scheduledAction)
        {
            case NeighbourAction.Continue:
                return scheduledAction;
            case NeighbourAction.Leave:
                gardener.OnAllActions();
                break;
            case NeighbourAction.ComeHome:
                gardener.OnAllActions();
                break;
            case NeighbourAction.GoInside:
                gardener.OnAllActions();
                break;
            case NeighbourAction.GoToSleep:
                gardener.OnAllActions();
                break;
            case NeighbourAction.Wakeup:
                gardener.OnAllActions();
                break;
            case NeighbourAction.GoOutside:
                gardener.OnAllActions();
                break;
            case NeighbourAction.EnterGarden:
                gardener.OnAllActions();
                break;
            case NeighbourAction.EnterYard:
                gardener.OnAllActions();
                break;
            case NeighbourAction.LeaveYard:
                gardener.OnAllActions();
                break;
            case NeighbourAction.LeaveGarden:
                gardener.OnAllActions();
                break;
            default:
                Debug.LogError($"No action assigned to NeighbourAction {scheduledAction}");
                break;
        }
        return scheduledAction;
    }
}
