using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gardener : MonoBehaviour
{
    [Header("Personality")]
    public bool isPlayer = false;
    public string neighbourName = " ";
    public Personality personality;

    [Header("Gameobjects")]
    public Garden garden;
    public GameObject neighbourObject;
    public MovementNodes nodes;
    public DialogBox dialogBox;
    public GameObject gardenHitBox;

    [Header("Position for array map")]
    public Vector2Int position;


    [Header("Set in code")]
    public NeighbourPosition currentPosition = NeighbourPosition.InBed;
    public float gardenValue => garden.totalValue;
    public Schedule schedule => personality.schedule;

    private void Awake()
    {
        neighbourObject.transform.rotation = Quaternion.identity;
        neighbourObject.GetComponentInChildren<SpriteRenderer>().color = personality.neighbourColor;
        if(isPlayer)
        {
            currentPosition = NeighbourPosition.InYard;
        }
    }
    private void Start()
    {
        garden.Setup(this);
        TimeOfDay.timeChangedEvent.AddListener(TimeChanged);
    }


    public void TimeChanged()
    {
        if (isPlayer) return;
        currentAction = schedule.GetAction();
        schedule.DoScheduledAction(this);
    }

    private void MoveForAction()
    {
        MovementAction moveAction = nodes.GetMovementAction(currentAction);
        if (moveAction == null) return;
        ValidateAction(moveAction.requiredStartPosition);
        if (moveAction.nodes == null) return;
        if (moveAction.nodes[0] == neighbourObject.transform.position) moveAction.nodes.RemoveAt(0);

        currentPosition = moveAction.endPosition;
        StartCoroutine(StartMoving(moveAction.nodes));
    }
    private float GetMovementDistance(List<Vector3> positions, out List<float> movementDistances)
    {
        movementDistances = new List<float>();
        float totalDistance = 0;
        Vector3 previousPosition = transform.position;
        foreach (Vector3 position in positions)
        {
            float distance = Vector3.Distance(previousPosition, position);
            totalDistance += distance;
            previousPosition = position;
            movementDistances.Add(distance);
        }
        return totalDistance;
    }
    public IEnumerator StartMoving(List<Vector3> nodePositions)
    {
        if (isPlayer) Debug("Moving player");
        List<float> movementDistances;
        float totalDistance = GetMovementDistance(nodePositions, out movementDistances);
        Vector3 previousPos = neighbourObject.transform.position;
        for (int i = 0; i < nodePositions.Count; i++)
        {
            float distance = movementDistances[i];
            Vector3 positionToMoveTo = nodePositions[i];
            float percentOFWholeMovement = distance / totalDistance;
            if (isPlayer) Debug("Distance " + distance + " total distance " + totalDistance + " dist/total " + percentOFWholeMovement);
            float startTime = Time.time;
            float timeForMovement = TimeOfDay.animationTimePerPoint * percentOFWholeMovement;
            if (isPlayer) Debug("timeForMovement " + timeForMovement);
            float percentMoved;
            float timePassed = 0;
            yield return new WaitForEndOfFrame();
            while (timePassed < timeForMovement)
            {
                yield return new WaitForEndOfFrame();
                timePassed = Time.time - startTime;
                if (isPlayer) Debug("Time passed moving " + timePassed);
                percentMoved = timePassed / timeForMovement;
                if (isPlayer) Debug("Moved " + percentMoved);
                Vector3 newPosition = Vector3.Lerp(previousPos, positionToMoveTo, percentMoved);
                newPosition.z = neighbourObject.transform.position.z;
                neighbourObject.transform.position = newPosition;
            }
            positionToMoveTo.z = neighbourObject.transform.position.z;
            neighbourObject.transform.position = positionToMoveTo;
            previousPos = neighbourObject.transform.position;
        }
    }
    #region actions

    public NeighbourAction currentAction = NeighbourAction.Continue;

    string actionHeader => $"neighbour {neighbourName} attempted {currentAction}";

    public void OnAllActions()
    {
        dialogBox.StartDialogForAction(this);
        MoveForAction();
    }

    private void ValidateAction(NeighbourPosition requiredPosition) 
    {
        if (currentPosition != requiredPosition)
        {
            UnityEngine.Debug.LogError(actionHeader + $" Position: {currentPosition} Required: {requiredPosition} at schedule index {TimeOfDay.currentAction}");
        }
    }


    public void MoveToGarden()
    {
        Debug("Move to garden");
        GameManager.instance.MovePlayerToGarden(this);
    }
    #endregion

    private static void Debug(string message)
    {
        UnityEngine.Debug.Log($"{GlobalSettings.NeighbourDebugHeader} {message}");
    }
}
