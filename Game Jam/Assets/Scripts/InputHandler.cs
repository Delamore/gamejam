using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
public static class InputHandler
{

    public static UnityEvent leftMouseDown = new UnityEvent();
    public static ClickWithPositionEvent leftMouseDownWithPosition = new ClickWithPositionEvent();

    public static void CheckInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Input.mousePosition;

            leftMouseDown.Invoke();
            leftMouseDownWithPosition.Invoke(mousePosition);
        }
    }


}

public sealed class ClickWithPositionEvent : UnityEvent<Vector3> { }