using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionPointUI : MonoBehaviour
{
    public Text text;
    public int points => ActionPoints.actionPoints;
    public int maxPoints => ActionPoints.maxActionPoints;

    private void Awake()
    {
        SetDisplay();
    }
    public void ActionPointsRemoved(int count)
    {
        SetDisplay();
    }
    public void ActionPointsRefreshed()
    {
        SetDisplay();
    }
    public void ActionPointsEmptied()
    {
        SetDisplay();
    }
    void SetDisplay()
    {
        text.text = $"Points: {points} \nDay: {GameManager.instance.currentDay}";
    }
}
