using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NeighbourPosition
{
    InBed,
    Inside,
    OutsideDoor,
    InYard,
    NotHome,
    InGarden
}

public class MovementNodes : MonoBehaviour
{
    public Transform outsideDoor;
    public Transform inside;
    public Transform inBed;
    public Transform inGarden;
    public Transform notHome;
    public Transform inYard;


    public MovementAction GetMovementAction(NeighbourAction action)
    {
        List<Vector3> nodes = new List<Vector3>();
        switch (action)
        {
            case NeighbourAction.Continue:
                return null;
            case NeighbourAction.Leave:
                nodes.Add(notHome.position);
                return new MovementAction(NeighbourPosition.OutsideDoor, NeighbourPosition.NotHome, nodes);
            case NeighbourAction.ComeHome:
                nodes.Add(outsideDoor.position);
                return new MovementAction(NeighbourPosition.NotHome, NeighbourPosition.OutsideDoor, nodes);
            case NeighbourAction.GoInside:
                nodes.Add(inside.position);
                return new MovementAction(NeighbourPosition.OutsideDoor, NeighbourPosition.Inside, nodes);
            case NeighbourAction.GoToSleep:
                nodes.Add(inBed.position);
                return new MovementAction(NeighbourPosition.Inside, NeighbourPosition.InBed, nodes);
            //case NeighbourAction.Asleep:
            //    return null;
            case NeighbourAction.Wakeup:
                nodes.Add(inside.position);
                return new MovementAction(NeighbourPosition.InBed, NeighbourPosition.Inside, nodes);
            //case NeighbourAction.Inside:
            //    return null;
            case NeighbourAction.GoOutside:
                nodes.Add(outsideDoor.position);
                return new MovementAction(NeighbourPosition.Inside, NeighbourPosition.OutsideDoor, nodes);
            case NeighbourAction.EnterGarden:
                nodes.Add(inGarden.position);
                return new MovementAction(NeighbourPosition.InYard, NeighbourPosition.InGarden, nodes);
            case NeighbourAction.EnterYard:
                nodes.Add(inYard.position);
                return new MovementAction(NeighbourPosition.OutsideDoor, NeighbourPosition.InYard, nodes);
            case NeighbourAction.LeaveYard:
                nodes.Add(outsideDoor.position);
                return new MovementAction(NeighbourPosition.InYard, NeighbourPosition.OutsideDoor, nodes);
            case NeighbourAction.LeaveGarden:
                nodes.Add(inYard.position);
                return new MovementAction(NeighbourPosition.InGarden, NeighbourPosition.InYard, nodes);
        }
        throw new System.Exception($"{action} not implemented in GetNodesForAction");
    }
}
public class MovementAction
{
    public NeighbourPosition requiredStartPosition;
    public NeighbourPosition endPosition;
    public List<Vector3> nodes;
    public MovementAction(NeighbourPosition startPosition, NeighbourPosition endPosition, List<Vector3> nodes)
    {
        this.requiredStartPosition = startPosition;
        this.endPosition = endPosition;
        this.nodes = nodes;
    }
}
