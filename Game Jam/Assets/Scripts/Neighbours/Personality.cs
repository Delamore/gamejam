using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Personality", menuName = "Jam/Personality", order = 51)]
public class Personality : ScriptableObject
{
    public Schedule schedule;
    public Barks barks;

    public Color neighbourColor;
}
