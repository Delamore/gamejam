using UnityEngine;
using UnityEngine.UI;

public class SLDR_SFXVolume : MonoBehaviour
{
    private Slider slider;
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.RemoveAllListeners();
        slider.onValueChanged.AddListener(delegate { ValueChanged(); });
    }

    private void ValueChanged()
    {
        float value = slider.value;
        AudioManager.ChangeSFXVolume(value);
    }
}
