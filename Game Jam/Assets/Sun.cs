using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sun : MonoBehaviour
{
    public Gradient dayGradient;
    public Image img;
    float oldPercent = -1;
    public void Update()
    {
        if (TimeOfDay.percentOfDayPassed != oldPercent)
            SetPercentOfDayPassed(TimeOfDay.percentOfDayPassed);
    }

    public void SetPercentOfDayPassed(float percent)
    {
        float evaluatePercent = percent;
        if (percent > 0.5f)
            evaluatePercent = 0.5f - (percent - 0.5f);
        Color color = dayGradient.Evaluate(evaluatePercent);
        img.color = color;
        oldPercent = percent;
    }
}
