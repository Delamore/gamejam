using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenTile : MonoBehaviour
{
    public Plant containedPlant;

    public Vector2Int position;
    public Garden gardenContaining;


    List<ContextAction> actions;

    OnClick clickControl;

    public void Setup(Vector2Int position, Garden garden)
    {
        this.position = position;
        this.gardenContaining = garden;
        clickControl = GetComponent<OnClick>();
    }

    public void AddPlant(Plant plant)
    {
        if (containedPlant != null) throw new System.Exception("Attempted to add plant to garden tile already containing plant");
        containedPlant = plant;
        plant.transform.SetParent(transform);
        plant.transform.localPosition = new Vector3(0, 0, -0.2f);
        SetupFunctions();
        ToggleOnClickAvaliable(true);
    }

    public void RemovePlant()
    {
        if (containedPlant == null) throw new System.Exception("Attempted to remove plant from garden tile with no plant");
        containedPlant = null;
        ToggleOnClickAvaliable(false);
    }


    void SetupFunctions()
    {
        if (gardenContaining.owner.isPlayer) return;


        actions = new List<ContextAction>
            {
                ContextAction.DestroyActionForGardenTile(this),
                ContextAction.StealActionForGardenTile(this)
            };
    }

    void ToggleOnClickAvaliable(bool avaliable)
    {
        if (gardenContaining.owner.isPlayer) return;
        if (avaliable)
            clickControl.onClickWithPosition = new Action<Vector2>(OnClick);
        else
            clickControl.onClickWithPosition = null;
    }
    public void OnClick(Vector2 position)
    {
        if (GameManager.instance.ownerOfGardenPlayerIsIn != gardenContaining.owner) return;
        new ContextualMenu(actions, position);
    }


    public void Destroy() => AllActions.DestroyPlant(this);    
    public void Steal() => AllActions.StealPlant(this);


}
