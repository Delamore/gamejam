using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Clock : MonoBehaviour
{
    public Image sun;
    public Text digitalClock;
    float dist = 110;
    float heightMod = 80;
    float oldPercent = -1;
    public void Update()
    {
        SetPercentOfDayPassed();
        SetClock();
    }
    void SetClock()
    {
        digitalClock.text = $"{TimeOfDay.currentHour}:{TimeOfDay.currentMinute}";
    }
    public void SetPercentOfDayPassed()
    {
        float percent = TimeOfDay.percentOfDayPassed;
        float height = Mathf.Sin(percent * Mathf.PI) * heightMod;
        float x = percent * dist - dist/2;
        sun.transform.localPosition = new Vector3(x, height, 0);
        oldPercent = percent;
    }

}
