using UnityEngine;
using UnityEngine.UI;

public class BTN_MusicMute : MonoBehaviour
{
    bool muted = false;
    void Start()
    {
        Button buttonAttached = GetComponent<Button>();
        buttonAttached.onClick.RemoveAllListeners();
        buttonAttached.onClick.AddListener(delegate { Pressed(); });
    }
    private void Pressed()
    {
        AudioManager.ToggleMusicMute(!muted);
        muted = !muted;
    }
}
