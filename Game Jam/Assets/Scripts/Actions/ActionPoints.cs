using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionPoints 
{

    public static int actionPoints = GlobalSettings.maxActionPoints;
    public static int maxActionPoints => GlobalSettings.maxActionPoints;

    public static ActionPointUI ui => _ui ??(_ui = GameObject.FindObjectOfType< ActionPointUI>());
    private static ActionPointUI _ui;

    public static bool HasEnoughPoints(int requiredAmount)
    {
        return actionPoints - requiredAmount >= 0;
    }
    public static void SpendActionPoints(int amountSpent)
    {
        Debug($"Spending {amountSpent} action points");
        actionPoints -= amountSpent;
        ui.ActionPointsRemoved(amountSpent);
        if (actionPoints == 0)
            RanOutOfActionPoints();

        if (actionPoints < 0)
            throw new System.Exception("Ended up at negative action points");
    }
    public static void ResetActionPoints()
    {
        Debug($"Resetting action points");
        actionPoints = maxActionPoints;
        ui.ActionPointsRefreshed();
    }
    private static void RanOutOfActionPoints()
    {
        GameManager.instance.DayEnded();
        Debug("Ran out of action points");
    }


    public static void EmptyActionPoints()
    {
        actionPoints = 0;
        ui.ActionPointsEmptied();
    }

    private static void Debug(string message)
    {
        UnityEngine.Debug.Log($"{GlobalSettings.ActionPointsDebugHeader} {message}");
    }
}
