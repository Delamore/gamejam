using System;
using UnityEngine;
public class OnClick : MonoBehaviour
{
    public Action onClick;
    public Action<Vector2> onClickWithPosition;

    public void Clicked(Vector2 position)
    {
        if (GameManager.instance.InputBlocked()) return;
        Debug.Log("OnClick Clicked()");

        if (onClick != null)
            onClick.Invoke();
        if (onClickWithPosition != null)
            onClickWithPosition.Invoke(position);
    }
}
