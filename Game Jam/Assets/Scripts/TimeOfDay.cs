using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeOfDay : MonoBehaviour
{
    public static UnityEvent timeChangedEvent = new UnityEvent();

    public static int currentAction = 0;

    static float animationTimeForDay => GlobalSettings.timeToAnimateFullDayOver;
    static int maxPoints => GlobalSettings.maxActionPoints;
    static int hoursOfPlayPerDay => GlobalSettings.endHour - GlobalSettings.startHour;
    static float hoursPerPoint => (float)hoursOfPlayPerDay / (float)maxPoints;
    static float minutesPerPoint => hoursPerPoint * 60f;
    static int hoursNightLasts => 24 - GlobalSettings.endHour + GlobalSettings.startHour;

    public static float animationTimePerPoint => animationTimeForDay / (float)maxPoints;
    public static float percentOfPlayHoursPerPoint => 1f / maxPoints;

    public static float percentOfDayPassed = 0;
    public static float percentOfCurrentActionPassed = 0;

    public static int currentHour = GlobalSettings.startHour;
    public static int currentMinute = 0;

    public IEnumerator PassMultiplePointsOfTime(int amount)
    {
        for (int i = 0; i < amount; i++)
            yield return StartCoroutine(PassOnePointOfTime());
    }
    IEnumerator PassOnePointOfTime()
    {
        timeChangedEvent.Invoke();
        float startPercent = percentOfDayPassed;
        float startTime = Time.time;
        float endTime = Time.time + animationTimePerPoint;
        //Debug.Log("Start time " + startTime);
        //Debug.Log("End time " + endTime);
        //Debug.Log("time Per Point " + animationTimePerPoint);
        float timePassed = 0;
        percentOfCurrentActionPassed = 0;
        while (timePassed < animationTimePerPoint)
        {
            timePassed = Time.time - startTime;
            //Debug.Log("Time passed " + timePassed);
            percentOfCurrentActionPassed = timePassed / animationTimePerPoint;
            Debug.Log("percentOfCurrentActionPassed " + percentOfCurrentActionPassed);
            SetPercentOfDayPassed(startPercent + percentOfPlayHoursPerPoint * percentOfCurrentActionPassed);
            yield return new WaitForEndOfFrame();
            UpdateFullTime();
        }
        percentOfCurrentActionPassed = 1;
        currentAction++;
        Debug.Log(currentAction);
        if (currentAction == maxPoints)
        { 
            currentMinute = 0;
            currentHour = GlobalSettings.endHour;
        }
    }
    public void NewMorning()
    {
        currentAction = 0;
        currentMinute = 0;
        currentHour = GlobalSettings.startHour;
    }
    public void SetPercentOfDayPassed(float percent)
    {
        if (percent >= 1) 
            percent = 0;
        percentOfDayPassed = percent;
    }
    public void UpdateFullTime()
    {
        float totalMinutes = (currentAction + percentOfCurrentActionPassed) * minutesPerPoint;
        currentHour = GlobalSettings.startHour + (int)(totalMinutes / 60);
        currentMinute = (int)(totalMinutes % 60);
    }
    public static void UpdateTimeDuringNight(float percentOfNightPassed)
    {
        float hoursIntoNight = (float)hoursNightLasts * percentOfNightPassed;
        int hour = GlobalSettings.endHour + Mathf.FloorToInt(hoursIntoNight);
        currentHour = hour % 24;
        currentMinute = (int)((hoursIntoNight % 1) * 60f);
    }
}
