using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    public Text dialogText;
    Vector3 defaultPosition;
    float defaultZ;
    float xBounds = 7.8f;
    float yBounds = 4.4f;
    private void Awake()
    {
        defaultPosition = transform.localPosition;
        defaultZ = transform.position.z;
        HideDialog();
    }
    private void LateUpdate()
    {
        LockPositionToScreenBounds();
    }
    private void Update()
    {
        LockPositionToScreenBounds();
    }
    private void FixedUpdate()
    {
        LockPositionToScreenBounds();
    }
    private void LockPositionToScreenBounds()
    {
        bool xPosGood = false;
        bool yPosGood = false;
        Vector3 pos = transform.position;
        pos.z = defaultZ;
        if (pos.x >= xBounds) pos.x = xBounds;
        else if (pos.x <= -xBounds) pos.x = -xBounds;
        else xPosGood = true;

        if (pos.y >= yBounds) pos.y = yBounds;
        else if (pos.y <= -yBounds) pos.y = -yBounds;
        else yPosGood = true;

        if (xPosGood && yPosGood)
            transform.localPosition = defaultPosition;
        else
            transform.position = pos;
    }
    public void StartDialogForAction(Gardener gardener)
    {
        string dialog = gardener.personality.barks.GetBarkForAction(gardener.currentAction, gardener);
        if (dialog == null) return;
        SetDialog(dialog);
    }

    public void SetDialog(string dialog)
    {
        ShowDialog();
        StartCoroutine(StartDialog(dialog));
    }
    IEnumerator StartDialog(string dialog)
    {
        float timeToDisplayFor = TimeOfDay.animationTimePerPoint;
        dialogText.text = dialog;
        yield return new WaitForSeconds(timeToDisplayFor);

        HideDialog();
    }
    void ShowDialog()
    {
        gameObject.SetActive(true);
    }
    void HideDialog()
    {
        dialogText.text = "";
        gameObject.SetActive(false);
    }
}
