﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
public enum SFX
{
    BGM1,
    Hit
}


public class AudioManager : MonoBehaviour
{
    static private AudioManager instance => _instance ?? (_instance = FindObjectOfType<AudioManager>());
    static private AudioManager _instance;

    static private AudioSource oneShotAudioSource
    {
        get
        {
            if (_oneShotAudioSource == null)
                _oneShotAudioSource = instance.gameObject.AddComponent<AudioSource>();
            return _oneShotAudioSource;
        }
    }
    static private AudioSource _oneShotAudioSource;

    static private AudioSource loopingAudioSource
    {
        get
        {
            if (_loopingAudioSource == null)
                _loopingAudioSource = instance.gameObject.AddComponent<AudioSource>();
            return _loopingAudioSource;
        }
    }
    static private AudioSource _loopingAudioSource;

    static private AudioSource musicAudioSource
    {
        get
        {
            if (_musicAudioSource == null)
                _musicAudioSource = instance.gameObject.AddComponent<AudioSource>();
            return _musicAudioSource;
        }
    }
    static private AudioSource _musicAudioSource;


    public static void PlayOneShot(SFX SFXName)
    {
        oneShotAudioSource.PlayOneShot(LoadSFX(SFXName), GlobalSettings.SFXVolume);
    }

    public static void PlayOneShotOnLoop(SFX SFXname, float timeToPlay)
    {
        instance.StopCoroutine("PlayOneShotOnLoop");
        instance.StartCoroutine(PlayOneShotOnLoop(LoadSFX(SFXname), timeToPlay));
    }

    private static IEnumerator PlayOneShotOnLoop(AudioClip clip, float timeToPlay)
    {
        if (loopingAudioSource.isPlaying)
            throw new System.Exception("Attempting to loop multiple audio at once");
        float endTime = Time.time + timeToPlay;


        //Debug.Log("First Play");
        loopingAudioSource.clip = clip;
        loopingAudioSource.time = 0f;
        loopingAudioSource.loop = true;
        loopingAudioSource.volume = GlobalSettings.SFXVolume;
        loopingAudioSource.Play();
        while (Time.time < endTime)
        {
            yield return null;
        }
        loopingAudioSource.Stop();
        //Debug.Log("End");
    }
    public static void RestartMusic()
    {
        Debug("Restarting BGM");
        musicAudioSource.volume = GlobalSettings.musicVolume;
        musicAudioSource.Stop();
        musicAudioSource.Play();
    }

    public static void StartMusic(SFX SFXName)
    {
        Debug("Starting BGM");
        musicAudioSource.clip = LoadSFX(SFXName);
        RestartMusic();
    }


    #region volume and muting

    public static void ChangeSFXVolume(float newVolume)
    {
        Debug($"Setting SFX volume to {newVolume}");
        GlobalSettings.SFXVolume = newVolume;
        loopingAudioSource.volume = newVolume;
        oneShotAudioSource.volume = newVolume;
    }

    public static void ChangeMusicVolume(float newVolume)
    {
        Debug($"Setting music volume to {newVolume}");
        GlobalSettings.musicVolume = newVolume;
        musicAudioSource.volume = newVolume;
    }

    public static void ToggleSFXMute(bool muted)
    {
        Debug($"Setting SFX muted to {muted}");
        if (muted)
        {
            loopingAudioSource.volume = 0;
            oneShotAudioSource.volume = 0;
        }
        else
        {
            loopingAudioSource.volume = GlobalSettings.SFXVolume;
            oneShotAudioSource.volume = GlobalSettings.SFXVolume;
        }
    }

    public static void ToggleMusicMute(bool muted)
    {
        Debug($"Setting music muted to {muted}");
        if (muted)
            musicAudioSource.volume = 0;

        else
            musicAudioSource.volume = GlobalSettings.musicVolume;
    }

    #endregion

    #region file loading

    public static string currentAudioDirectory = "Audio/";
    static public AudioClip LoadSFX(SFX SFXName)
    {
        //Debug.Log("Loading SFX " + SFXName.ToString());
        string filePath = currentAudioDirectory;
        string fileName = GetSFXFileName(SFXName);
        //Debug.Log("At file path " + filePath + fileName);
        AudioClip loadedClip = Resources.Load<AudioClip>(filePath + fileName);
        if (loadedClip == null)
            throw new System.Exception("Invalid clip at path " + filePath + fileName);
        return loadedClip;
    }
    public static string GetSFXFileName(SFX sfx)
    {
        return SFXFileNames[sfx];
    }

    static public Dictionary<SFX, string> SFXFileNames = new Dictionary<SFX, string>()
    {
        {SFX.BGM1, "BGM/BGM"},
        {SFX.Hit, "hit"}
    };

    #endregion

    private static void Debug(string message)
    {
        UnityEngine.Debug.Log($"{GlobalSettings.AudioDebugHeader} {message}");
    }
}
