using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GardenDebug : MonoBehaviour
{
    public Gardener gardener;
    public Text text;

    // Update is called once per frame
    void Update()
    {
        text.text = $"{gardener.name} \nGarden Value: {gardener.gardenValue} \nPlants {gardener.garden.totalPlants}\nAction {gardener.currentAction}";
    }
}
