using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeNight : MonoBehaviour
{
    public Image img;

    float fadeToDarkTime => GlobalSettings.fadeToDarkTime;
    float holdDarkTime => GlobalSettings.holdDarkTime;
    float fadeToDayTime => GlobalSettings.fadeToDayTime;

    public float totalFadeTime => fadeToDarkTime + holdDarkTime + fadeToDayTime;

    public IEnumerator Fade()
    {
        float startTime = Time.time;

        yield return StartCoroutine(FadeOut(startTime));
        yield return StartCoroutine(HoldNight(startTime));
        yield return StartCoroutine(FadeIn(startTime));
    }
    private IEnumerator FadeOut(float startOfNightTime)
    {
        Color color = Color.black;
        float startTime = Time.time;
        float endPoint = startTime + fadeToDarkTime;

        float currentTime = startTime;
        float percentPassed;

        while (currentTime < endPoint)
        {
            currentTime = Time.time;
            percentPassed = (currentTime - startTime) / fadeToDarkTime;
            color.a = percentPassed;
            img.color = color;
            yield return new WaitForEndOfFrame();

            TimeOfDay.UpdateTimeDuringNight((currentTime - startOfNightTime) / totalFadeTime);
        }
    }
    private IEnumerator HoldNight(float startOfNightTime)
    {
        float startTime = Time.time;
        float endPoint = startTime + holdDarkTime;

        float currentTime = startTime;
        while (currentTime < endPoint)
        {
            currentTime = Time.time;
            yield return new WaitForEndOfFrame();
            TimeOfDay.UpdateTimeDuringNight((currentTime - startOfNightTime) / totalFadeTime);
        }
    }
    private IEnumerator FadeIn(float startOfNightTime)
    {
        Color color = Color.black;
        float startTime = Time.time;
        float endPoint = startTime + fadeToDayTime;

        float currentTime = startTime;
        float percentPassed;

        while (currentTime < endPoint)
        {
            currentTime = Time.time;
            percentPassed = (currentTime - startTime) / fadeToDayTime;
            color.a = 1 - percentPassed;
            img.color = color;

            yield return new WaitForEndOfFrame();

            TimeOfDay.UpdateTimeDuringNight((currentTime - startOfNightTime) / totalFadeTime);
        }
    }
}
