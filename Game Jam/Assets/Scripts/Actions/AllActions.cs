using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class AllActions
{
    #region playerActions
    public static void ActionInvoked(ContextAction contextAction)
    {
        GameManager.instance.SpentPoints(contextAction.actionPointCost);
        ActionPoints.SpendActionPoints(contextAction.actionPointCost);
    }
    public static void StealPlant(GardenTile tile)
    {
        Plant plant = tile.containedPlant;
        tile.gardenContaining.RemovePlant(tile, false);
        GameManager.instance.playerGardener.garden.AddPlant(plant);
    }
    public static void DestroyPlant(GardenTile tile)
    {
        tile.gardenContaining.RemovePlant(tile, true);
    }

    #endregion

}
